'''
Program to order a list of words given as arguments
'''

import sys


def is_lower(first: str, second: str):
    lower = False
    for pos in range(len(first)):
        if pos < len(second):
            if first[pos].lower() < second[pos].lower():
                lower = True
                break
            if first[pos].lower() > second[pos].lower():
                break
    return lower


def sort_pivot(words: list, pos: int):
    less: int = pos
    for m in range(pos+1, len(words)):
        if is_lower(words[m], words[less]):
            m, less = less, m
    return less


def sort(words: list):
    for pivot in range(len(words)):
        lower = sort_pivot(words, pivot)
        if lower!= pivot:
            words[lower], words[pivot] = words[pivot], words[lower]
    return words


def show(words: list):
    for word in words:
        print(word, end=" ")
    print()


def main():
    words: list = sys.argv[1:]
    sort(words)
    show(words)


if __name__ == '__main__':
    main()
